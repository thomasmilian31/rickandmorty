package com.mysociality.rickandmorty.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mysociality.rickandmorty.ui.tabs.AllCharactersFragment
import com.mysociality.rickandmorty.ui.tabs.FavsFragment

class MainAdapter(
    fm: FragmentManager,
    private var totalTabs: Int
) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> AllCharactersFragment()
            else -> FavsFragment()
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }
}