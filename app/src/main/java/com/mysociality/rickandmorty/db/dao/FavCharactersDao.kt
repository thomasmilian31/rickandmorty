package com.mysociality.rickandmorty.db.dao

import com.mysociality.rickandmorty.db.objectbox.ObjectBox
import com.mysociality.rickandmorty.model.Character
import io.objectbox.Box
import io.objectbox.android.AndroidScheduler
import io.objectbox.kotlin.boxFor
import io.objectbox.reactive.DataObserver
import io.objectbox.reactive.DataSubscription
import org.koin.core.KoinComponent

class FavCharactersDao: KoinComponent {

    private val box: Box<Character> = ObjectBox.boxStore.boxFor()

    fun getFavsCharacters(observer : DataObserver<List<Character>>): DataSubscription {
        val query = box.query().build()
        return query.subscribe()
            .on(AndroidScheduler.mainThread())
            .observer(observer)
    }

    fun addFavCharacter(character: Character) {
        box.put(character)
    }

    fun removeFavCharacter(character: Character) {
        box.remove(character)
    }
}