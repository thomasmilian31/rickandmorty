# Installation guide

This section describes how to run the application on an Android phone from Android Studio

## Enable USB debugging

Please follow the steps described at the section [Enable USB debugging](https://guides.codepath.com/android/Running-Apps-on-Your-Device)

## Run on Android Studio

1. Clone the project on your computer
2. Connect your phone to your computer
3. Open Android Studio
3. Select "Open an existing Android studio project"
4. In the dialog, select the root folder of the project (default name is "RickAndMorty")
5. After Android Studio finished to build and index the project, ensure that your device is selected in the toolbar and click on the play button to run the project