package com.mysociality.rickandmorty.di

import com.mysociality.rickandmorty.repositories.CharacterRepository
import com.mysociality.rickandmorty.repositories.CharacterRepositoryImpl
import com.mysociality.rickandmorty.repositories.CharacterRetrofitInterface
import org.koin.dsl.module
import retrofit2.Retrofit

val repositoriesModule = module {
    single<CharacterRetrofitInterface> { get<Retrofit>().create(CharacterRetrofitInterface::class.java) }
    single<CharacterRepository> { CharacterRepositoryImpl(get()) }
}