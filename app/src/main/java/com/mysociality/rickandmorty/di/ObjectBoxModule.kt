package com.mysociality.rickandmorty.di

import com.mysociality.rickandmorty.db.dao.FavCharactersDao
import org.koin.dsl.module

val objectBoxModule = module {
    single { FavCharactersDao() }
}