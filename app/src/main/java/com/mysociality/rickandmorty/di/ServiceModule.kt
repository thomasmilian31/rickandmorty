package com.mysociality.rickandmorty.di

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.mysociality.rickandmorty.BuildConfig
import com.mysociality.rickandmorty.utils.CustomDateDeserializer
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

val serviceModule = module {
    single { createOkHttpClient() }
    single { createRetrofit(get()) }
}

fun createOkHttpClient(): OkHttpClient {
    val okHttpClientBuilder = OkHttpClient.Builder()
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)

    if (BuildConfig.DEBUG) {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        okHttpClientBuilder.addInterceptor(logging)
    }
    return okHttpClientBuilder.build()
}

private fun createRetrofit(okHttpClient: OkHttpClient): Retrofit {

    val base = BuildConfig.SERVER_URL
    val objectMapper: ObjectMapper = jacksonObjectMapper().apply {
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        val simpleModule = SimpleModule()
        simpleModule.addDeserializer(Date::class.java, CustomDateDeserializer())
        registerModule(simpleModule)
    }
    val retrofitBuilder = Retrofit.Builder()
        .baseUrl(base)
        .addConverterFactory(JacksonConverterFactory.create(objectMapper))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpClient)

    return retrofitBuilder.build()
}