package com.mysociality.rickandmorty.ui.tabs

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.mysociality.rickandmorty.R
import com.mysociality.rickandmorty.utils.MarginItemDecoration
import com.mysociality.rickandmorty.utils.px
import com.mysociality.rickandmorty.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.recycler_view_fragment.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class FavsFragment: Fragment(), CardListener {

    private val mainViewModel: MainViewModel by sharedViewModel()

    private val cardAdapter by lazy { CardAdapter(this, emptyList()) }

    private var isLoading = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recycler_view_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.apply {
            setHasFixedSize(true)
            addItemDecoration(MarginItemDecoration(20.px, 15.px))
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = cardAdapter
        }
        errorTV.text = context?.resources?.getString(R.string.no_favs)
        setCallbacks()
    }

    private fun setCallbacks() {
        mainViewModel.favCharacters.observe(viewLifecycleOwner, Observer {
            errorTV.visibility = if (it.isEmpty()) View.VISIBLE else View.GONE
            val new = it
            val old = cardAdapter.getCharacters()
            val callback = CharacterDiffCallback(old, new)
            val result = DiffUtil.calculateDiff(callback)
            cardAdapter.setCharacters(new)
            result.dispatchUpdatesTo(cardAdapter)
            isLoading = false
        })
        mainViewModel.isFavsLoaded.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressBar.visibility = View.GONE
            }
        })
    }

    override fun changeFavStatus(cardId: Long) {
        val context = recyclerView.context
        val builder = AlertDialog.Builder(context)
        builder.setMessage(context.resources.getString(R.string.are_you_sure_delete))
        builder.setCancelable(true)
        builder.setPositiveButton(R.string.yes) { _, _ ->
            mainViewModel.changeStatusCharacter(cardId)
        }
        builder.setNegativeButton(R.string.no,null)
        builder.create()
        builder.show()
    }
}