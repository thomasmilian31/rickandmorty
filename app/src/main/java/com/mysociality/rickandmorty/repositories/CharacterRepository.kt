package com.mysociality.rickandmorty.repositories

import com.mysociality.rickandmorty.model.Characters
import io.reactivex.Single

interface CharacterRepository {
    fun getCharacters(page: Int): Single<Characters>
}