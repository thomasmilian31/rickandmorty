package com.mysociality.rickandmorty.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayout
import com.mysociality.rickandmorty.R
import com.mysociality.rickandmorty.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.viewmodel.ext.android.sharedViewModel

class MainFragment: Fragment() {

    private val mainViewModel: MainViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTabLayout()
        mainViewModel.getFavCharacters()
        mainViewModel.getCharacters()
        setCallbacks()
    }

    private fun setTabLayout() {
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = MainAdapter(childFragmentManager, tabLayout.tabCount)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    viewPager.currentItem = it.position
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }
        })
    }

    private fun setCallbacks() {
        mainViewModel.favStatusChanged.observe(viewLifecycleOwner, Observer {pair ->
            val string =
                if (pair.second)
                    getString(R.string.favs_added, pair.first)
                else
                    getString(R.string.favs_deleted, pair.first)
            Toast.makeText(context, string, Toast.LENGTH_LONG).show()
        })
    }
}