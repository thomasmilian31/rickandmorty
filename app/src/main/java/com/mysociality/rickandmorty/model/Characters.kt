package com.mysociality.rickandmorty.model

data class Characters(
    var info:Info,
    var results: List<Character>
)