package com.mysociality.rickandmorty.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mysociality.rickandmorty.db.dao.FavCharactersDao
import com.mysociality.rickandmorty.model.Character
import com.mysociality.rickandmorty.repositories.CharacterRepository
import io.objectbox.reactive.DataObserver
import io.objectbox.reactive.DataSubscriptionList
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainViewModel(
    private val repository: CharacterRepository,
    private val favCharactersDao: FavCharactersDao
): ViewModel() {

    private val disposables = CompositeDisposable()

    private val subscriptions = DataSubscriptionList()

    private var page = 1

    val characters = MutableLiveData<List<Character>>()

    val favCharacters = MutableLiveData<List<Character>>()

    val error = MutableLiveData<Boolean>()

    val isCharactersLoaded = MutableLiveData<Boolean>()

    val isFavsLoaded = MutableLiveData<Boolean>()

    val favStatusChanged = MutableLiveData<Pair<String, Boolean>>()

    fun getCharacters() {
        disposables.add(
            repository.getCharacters(page)
                .map {
                    val favCharacters = favCharacters.value ?: emptyList()
                    val allCharacters = (characters.value ?: emptyList()).toMutableList()
                    allCharacters.addAll(it.results)
                    concatResults(allCharacters.toList(), favCharacters)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    characters.postValue(it.first)
                    favCharacters.postValue(it.second)
                    isCharactersLoaded.postValue(true)
                    error.postValue(false)
                    page += 1
                }, {
                    isCharactersLoaded.postValue(true)
                    error.postValue(true)
                })
        )
    }

    fun getFavCharacters() {
        subscriptions.add(
            favCharactersDao.getFavsCharacters(DataObserver {
                val charactersList = characters.value ?: emptyList()
                val pair = concatResults(charactersList, it)
                characters.postValue(pair.first)
                favCharacters.postValue(pair.second)
                isFavsLoaded.postValue(true)
            })
        )
    }

    fun changeStatusCharacter(id: Long) {
        val allCharacters = characters.value ?: emptyList()
        allCharacters.find { it.id == id }?.let {
            updateCharacterStatus(it)
        } ?: run {
            val favs = favCharacters.value ?: emptyList()
            favs.find { it.id == id }?.let {
                updateCharacterStatus(it)
            }
        }
    }

    private fun updateCharacterStatus(character: Character) {
        character.isFav = !character.isFav
        if (character.isFav) {
            favCharactersDao.addFavCharacter(character)
            favStatusChanged.postValue(Pair(character.name, true))
        } else {
            favCharactersDao.removeFavCharacter(character)
            favStatusChanged.postValue(Pair(character.name, false))
        }
    }

    private fun concatResults(
        allCharacters: List<Character>,
        favCharacters: List<Character>
    ): Pair<List<Character>, List<Character>> {
        val characters = allCharacters.map {character ->
            character.isFav = favCharacters.any { it.id == character.id }
            character
        }
        return Pair(characters, favCharacters)
    }
}