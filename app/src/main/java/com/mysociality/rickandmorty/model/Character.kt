package com.mysociality.rickandmorty.model

import com.mysociality.rickandmorty.R
import com.mysociality.rickandmorty.db.objectbox.*
import io.objectbox.annotation.Convert
import io.objectbox.annotation.Entity
import io.objectbox.annotation.Id

@Entity
data class Character(
    @Id(assignable = true)
    var id:Long,
    var name:String,
    @Convert(converter = StatusConverter::class, dbType = String::class)
    var status:Status,
    var species:String,
    var type:String,
    @Convert(converter = GenderConverter::class, dbType = String::class)
    var gender:Gender,
    @Convert(converter = OriginConverter::class, dbType = String::class)
    var origin:Origin,
    @Convert(converter = LocationConverter::class, dbType = String::class)
    var location: Location,
    var image:String,
    @Convert(converter = StringListConverter::class, dbType = String::class)
    var episode:List<String>,
    var url:String,
    var created:String,
    var isFav:Boolean = false
)

enum class Status {
    Alive,
    Dead,
    unknown
}

fun Status.color() : Int {
    return when (this) {
        Status.Alive -> R.color.green
        Status.Dead -> R.color.red
        Status.unknown -> R.color.light_gray
    }
}

enum class Gender {
    Female,
    Male,
    Genderless,
    unknown
}

data class Origin(
    var name:String,
    var url:String
)

data class Location(
    var name:String,
    var url:String
)