package com.mysociality.rickandmorty.di

import com.mysociality.rickandmorty.viewmodel.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { MainViewModel(get(), get()) }
}

