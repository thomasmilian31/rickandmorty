package com.mysociality.rickandmorty.db.objectbox

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.mysociality.rickandmorty.model.Gender
import com.mysociality.rickandmorty.model.Location
import com.mysociality.rickandmorty.model.Origin
import com.mysociality.rickandmorty.model.Status
import io.objectbox.converter.PropertyConverter

class LocationConverter: PropertyConverter<Location?, String?> {
    override fun convertToDatabaseValue(entityProperty: Location?): String? {
        entityProperty?.let {
            return jacksonObjectMapper().writeValueAsString(it)
        }
        return null
    }

    override fun convertToEntityProperty(databaseValue: String?): Location? {
        databaseValue?.let {
            return jacksonObjectMapper().readValue(it, Location::class.java)
        }
        return null
    }
}

class OriginConverter: PropertyConverter<Origin?, String?> {
    override fun convertToDatabaseValue(entityProperty: Origin?): String? {
        entityProperty?.let {
            return jacksonObjectMapper().writeValueAsString(it)
        }
        return null
    }

    override fun convertToEntityProperty(databaseValue: String?): Origin? {
        databaseValue?.let {
            return jacksonObjectMapper().readValue(it, Origin::class.java)
        }
        return null
    }
}

class GenderConverter: PropertyConverter<Gender?, String?> {
    override fun convertToDatabaseValue(entityProperty: Gender?): String? {
        entityProperty?.let {
            return it.name
        }
        return null
    }

    override fun convertToEntityProperty(databaseValue: String?): Gender? {
        databaseValue?.let {
            return Gender.valueOf(it)
        }
        return null
    }
}

class StatusConverter: PropertyConverter<Status?, String?> {
    override fun convertToDatabaseValue(entityProperty: Status?): String? {
        entityProperty?.let {
            return it.name
        }
        return null
    }

    override fun convertToEntityProperty(databaseValue: String?): Status? {
        databaseValue?.let {
            return Status.valueOf(it)
        }
        return null
    }
}

class StringListConverter: PropertyConverter<List<String>?, String?> {
    override fun convertToDatabaseValue(entityProperty: List<String>?): String? {
        entityProperty?.let {
            return jacksonObjectMapper().writeValueAsString(it)
        }
        return null
    }

    override fun convertToEntityProperty(databaseValue: String?): List<String>? {
        databaseValue?.let {
            return jacksonObjectMapper().readValue(it)
        }
        return null
    }
}