package com.mysociality.rickandmorty.repositories

import com.mysociality.rickandmorty.model.Characters
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

class CharacterRepositoryImpl(
    private val characterService: CharacterRetrofitInterface
) : CharacterRepository {
    override fun getCharacters(page: Int): Single<Characters> {
        return characterService.getCharacters(page)
    }
}

interface CharacterRetrofitInterface {
    @GET("character/")
    fun getCharacters(@Query("page") page: Int): Single<Characters>
}