package com.mysociality.rickandmorty.utils

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

internal class CustomDateDeserializer : JsonDeserializer<Date>() {

    private val dateFormats = arrayOf("yyyy-MM-dd HH:mm:ss.fff")

    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): Date {
        p?.let {jsonParser ->
            val dateString = jsonParser.text
            for (format in dateFormats) {
                try {
                    val date = SimpleDateFormat(format, Locale.getDefault()).parse(dateString)
                    date?.let {
                        return it
                    }
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            }
        }
        return Date(0)
    }
}