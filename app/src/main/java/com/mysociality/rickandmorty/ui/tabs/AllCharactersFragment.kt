package com.mysociality.rickandmorty.ui.tabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mysociality.rickandmorty.R
import com.mysociality.rickandmorty.utils.MarginItemDecoration
import com.mysociality.rickandmorty.utils.px
import com.mysociality.rickandmorty.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.recycler_view_fragment.*
import org.koin.android.viewmodel.ext.android.sharedViewModel


class AllCharactersFragment: Fragment(), CardListener {

    private val mainViewModel: MainViewModel by sharedViewModel()

    private val cardAdapter by lazy { CardAdapter(this, emptyList()) }

    private var isLoading = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recycler_view_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView.apply {
            setHasFixedSize(true)
            addItemDecoration(MarginItemDecoration(20.px, 15.px))
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = cardAdapter
        }
        errorTV.text = context?.resources?.getString(R.string.error_network)
        setCallbacks()
        setListeners()
    }

    private fun setListeners() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
                    ?: return
                if (!isLoading &&
                    linearLayoutManager.findLastCompletelyVisibleItemPosition() ==
                    cardAdapter.getCharacters().size - 1
                ) {
                    mainViewModel.getCharacters()
                    isLoading = true
                }
            }
        })
        refreshButton.setOnClickListener {
            mainViewModel.getCharacters()
        }
    }

    private fun setCallbacks() {
        mainViewModel.characters.observe(viewLifecycleOwner, Observer {newCharacters ->
            val old = cardAdapter.getCharacters()
            val new = newCharacters.map { it.copy() }
            val callback = CharacterDiffCallback(old, new)
            val result = DiffUtil.calculateDiff(callback)
            result.dispatchUpdatesTo(cardAdapter)
            cardAdapter.setCharacters(new)
            isLoading = false
        })
        mainViewModel.isCharactersLoaded.observe(viewLifecycleOwner, Observer {
            if (it) {
                progressBar.visibility = View.GONE
            }
        })
        mainViewModel.error.observe(viewLifecycleOwner, Observer {
            val visibility = if (it) View.VISIBLE else View.GONE
            errorTV.visibility = visibility
            refreshButton.visibility = visibility
        })
    }

    override fun changeFavStatus(cardId: Long) {
        mainViewModel.changeStatusCharacter(cardId)
    }
}