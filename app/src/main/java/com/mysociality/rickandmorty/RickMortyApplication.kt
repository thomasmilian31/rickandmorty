package com.mysociality.rickandmorty

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.mysociality.rickandmorty.db.objectbox.ObjectBox
import com.mysociality.rickandmorty.di.appModule
import com.mysociality.rickandmorty.di.objectBoxModule
import com.mysociality.rickandmorty.di.repositoriesModule
import com.mysociality.rickandmorty.di.serviceModule
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class RickMortyApplication: Application() {

    private val implModules = listOf(
        objectBoxModule,
        serviceModule,
        repositoriesModule,
        appModule
    )

    override fun onCreate() {
        super.onCreate()

        ObjectBox.init(this)
        Fresco.initialize(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        RxJavaPlugins.setErrorHandler {
            Timber.e("Rx Exception could not be delivered: $it")
        }
        startKoin {
            androidLogger()
            androidContext(this@RickMortyApplication)
            environmentProperties()
            modules(implModules)
        }
    }
}