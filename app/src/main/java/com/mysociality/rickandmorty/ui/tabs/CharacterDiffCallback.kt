package com.mysociality.rickandmorty.ui.tabs

import androidx.recyclerview.widget.DiffUtil
import com.mysociality.rickandmorty.model.Character
import timber.log.Timber

class CharacterDiffCallback(
    private val old: List<Character>,
    private val new: List<Character>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return old.size
    }

    override fun getNewListSize(): Int {
        return new.size
    }

    override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition].id == new[newPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val isSame = old[oldPosition].isFav == new[newPosition].isFav
        Timber.e("Is the same : %s", isSame)
        return isSame
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return 1
    }
}