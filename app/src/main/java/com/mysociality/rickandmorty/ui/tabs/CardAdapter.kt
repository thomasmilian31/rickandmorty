package com.mysociality.rickandmorty.ui.tabs

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.material.card.MaterialCardView
import com.google.android.material.textview.MaterialTextView
import com.mysociality.rickandmorty.R
import com.mysociality.rickandmorty.model.Character
import com.mysociality.rickandmorty.model.color
import kotlinx.android.synthetic.main.card_item.view.*
import timber.log.Timber

interface CardListener {
    fun changeFavStatus(cardId: Long)
}

class CardAdapter(
    private val listener:CardListener,
    private var characters: List<Character> = emptyList()
) : RecyclerView.Adapter<CardAdapter.CardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CardViewHolder(inflater.inflate(R.layout.card_item, parent, false))
    }

    override fun onBindViewHolder(
        holder: CardViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        super.onBindViewHolder(holder, position, payloads)
        if (payloads.isEmpty()) {
            onBindViewHolder(holder, position)
        } else {
            Timber.e("update")
            val character = characters[position]
            holder.favButton.setImageResource(
                if (character.isFav) R.drawable.ic_star_on else R.drawable.ic_star_off
            )
        }
    }
    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val character = characters[position]
        val context = holder.imageView.context

        holder.imageView.setImageURI(character.image)
        holder.nameTV.text = character.name
        var status = character.status.name.capitalize()
        status += " - "
        status += character.species.capitalize()
        holder.statusTV.text = status
        val backgroundColor = ContextCompat.getColor(context, character.status.color())
        holder.statusIndicator.setCardBackgroundColor(backgroundColor)
        holder.lastKnownLocation.text = character.location.name
        holder.firstSeen.text = character.origin.name
        holder.favButton.setImageResource(
            if (character.isFav) R.drawable.ic_star_on else R.drawable.ic_star_off
        )

        holder.favButton.setOnClickListener {
            listener.changeFavStatus(character.id)
        }
    }

    override fun getItemCount(): Int {
        return characters.size
    }

    fun setCharacters(characters: List<Character>) {
        this.characters = characters
    }

    fun getCharacters(): List<Character> {
        return characters
    }

    class CardViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView:SimpleDraweeView = view.imageView
        val nameTV: MaterialTextView = view.nameTV
        val statusIndicator: MaterialCardView = view.statusIndicator
        val statusTV: MaterialTextView = view.statusTV
        val lastKnownLocation: MaterialTextView = view.lastKnownLocation
        val firstSeen: MaterialTextView = view.firstSeen
        val favButton: ImageButton = view.favButton
    }
}